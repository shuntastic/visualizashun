import AudioHandler from '../utils/AudioHandler';
import audioFile from '/assets/aud/BlueLace.mp3';

class LineExerciseOne {
  constructor(props) {
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;
    // this.colorOpts = [
    //   { r: 255, g: 227, b: 255, hex: 0x518084 },
    //   { r: 178, g: 141, b: 178, hex: 0xd9afc7 },
    //   { r: 255, g: 243, b: 201, hex: 0x9e9660 },
    //   { r: 161, g: 204, b: 198, hex: 0x847f57 },
    //   { r: 150, g: 178, b: 174, hex: 0x3a5051 }
    // ];

    const vals = {
      src: audioFile
    };
    this.audAlyzer = new AudioHandler(vals);
    this.startBtn.addEventListener('click', evt => {
      this.init();
    });
  }

  init() {
    // this.startBtn.removeEventListener('click');
    this.audAlyzer.initAndStart();
    this.canvas = document.createElement('canvas');
    this.canvas.setAttribute('id', 'lineExerciseOne');
    this.canvas.classList.add('abs');
    this.canvas.classList.add('TL');
    this.canvas.classList.add('overlay');
    this.updateCanvas();
    document.body.appendChild(this.canvas);
    this.ctx = this.canvas.getContext('2d');
  
    this.renderFrame();
  }

  updateCanvas() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.WIDTH = window.innerWidth;
    this.HEIGHT = window.innerHeight;
  }

  animateAudio() {
    this.updateCanvas();
    this.audAlyzer.analyser.getByteFrequencyData(this.audAlyzer.dataArray);

    // this.ctx.fillStyle = 'rgba(0,0,0,.5)';
    this.ctx.globalCompositeOperation = 'destination-out';
    this.ctx.fillStyle = 'rgba(0, 0, 0, 0)';
    this.ctx.fillRect(0, 0, this.WIDTH, this.HEIGHT);
    this.ctx.globalCompositeOperation = 'source-over';

    this.ctx.fillRect(0, 0, this.WIDTH, this.HEIGHT);
    this.startX = 0;
    for (var i = 0; i < this.audAlyzer.bufferLength; i++) {
      let barWidth = this.audAlyzer.dataArray[i] * 0.025;

      // i == 0 ? console.log(i, dataArray[i]) : null;
      //   var r = this.HEIGHT + 25 * (i / bufferLength);
      //   var g = 250 * (i / bufferLength);
      //   var b = 50;
      var alphaVal = this.audAlyzer.dataArray[i] / 255;
      this.ctx.fillStyle = 'rgba(255,255,255,' + alphaVal + ')';
      // ctx.fillStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
      this.ctx.fillRect(this.startX, 0, barWidth, this.HEIGHT);

      // x += WIDTH / bufferLength * 8;
      this.startX += (this.WIDTH / this.audAlyzer.bufferLength) * 1.3;
    }
  }

  renderFrame() {
    requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
  }
}
export default LineExerciseOne;
