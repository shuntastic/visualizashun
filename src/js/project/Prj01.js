(function(root, factory) {
  'use strict';
  root.sections = root.sections || {};

  if (typeof define === 'function' && define.amd) {
    define(
      [
        'jquery',
        'greensock/TweenMax.min',
        'greensock/TimelineMax.min',
        'greensock/easing/EasePack.min',
        'packages/Sequencer'
      ],
      function($, TweenMax, TimelineMax, EasePack, Sequencer) {
        var o = factory($);
        return (root.sections.prj01 = o);
      }
    );
  } else {
    var o = factory($);
    root.sections.prj01 = o;
  }
})((window.SITE = window.SITE || {}), function() {
  var file, audio;
  var context, src, analyser, bufferLength, dataArray, canvas, ctx, WIDTH, HEIGHT, barWidth, barHeight;

  function Prj01() {}
  function enter() {}
  function exit() {}

  function playStream(audioStream) {
    var uInt8Array = new Uint8Array(audioStream);
    var arrayBuffer = uInt8Array.buffer;
    var blob = new Blob([arrayBuffer]);
    var url = URL.createObjectURL(blob);

    audioElement.src = url;
    audioElement.play();
  }
  function animateAudio() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    WIDTH = canvas.width;
    HEIGHT = canvas.height;
    // barWidth = WIDTH / bufferLength * 1;
    // barHeight;
    x = 0;

    analyser.getByteFrequencyData(dataArray);

    ctx.fillStyle = 'rgb(100,0,0)';
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    for (var i = 0; i < bufferLength; i++) {
      barWidth = dataArray[i] * 0.025;

      // i == 0 ? console.log(i, dataArray[i]) : null;
      var r = barHeight + 25 * (i / bufferLength);
      var g = 250 * (i / bufferLength);
      var b = 50;
      var alphaVal = dataArray[i] / 255;
      ctx.fillStyle = 'rgba(0,0,0,' + alphaVal + ')';
      // ctx.fillStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
      ctx.fillRect(x, 0, barWidth, HEIGHT);

      // x += WIDTH / bufferLength * 8;
      x += WIDTH / bufferLength * 1.3;
      // console.log('x:', x);
    }
  }

  function renderFrame() {
    requestAnimationFrame(renderFrame);
    animateAudio();
  }

  function init() {
    home.showPrj('prj01');
    // var files = document.getElementById('thefiles').files;
    // var uInt8Array = new Uint8Array();
    // var arrayBuffer = uInt8Array.buffer;
    // var blob = new Blob([arrayBuffer]);
    var audio = new Audio(SITE.dom + 'aud/RoadToPerdition.mp3');
    // audio.src = URL.createObjectURL();
    audio.load();
    audio.onended = function() {
      document.getElementById('shunLogo').className += 'done';
    };
    context = new AudioContext();
    src = context.createMediaElementSource(audio);
    analyser = context.createAnalyser();

    src.connect(analyser);
    analyser.connect(context.destination);

    analyser.fftSize = 256;

    bufferLength = analyser.frequencyBinCount;
    // console.log(bufferLength);

    dataArray = new Uint8Array(bufferLength);
    canvas = document.getElementById('canvas1');
    ctx = canvas.getContext('2d');
    audio.play();
    audio.volume = 1.0;
    renderFrame();
  }

  Prj01.prototype.init = init;
  Prj01.prototype.enter = enter;
  Prj01.prototype.exit = exit;

  return new Prj01();
});
