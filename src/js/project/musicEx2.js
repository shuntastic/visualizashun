import AudioHandler from '../utils/AudioHandler';
// import audioFile from '/assets/aud/Equipoise.mp3';
// 11 Fauré_ Pelléas Et Mélisande, Op. 80 - Sicilienne
import audioFile from '/assets/aud/Sicilienne.mp3';
// import audioFile from '/assets/aud/Basquiat_Score_-_J_Ralph_(Radiant_Child_(2010)_Ending_Theme).mp3';
// import logo from '/assets/images/shuniologo_w.svg';
// import '../../node_modules/createjs/builds/createjs-2015.11.26.min.js';
import regeneratorRuntime from 'regenerator-runtime';
// import * as THREE from 'three';
// import { SVGLoader } from '../libs/SVGLoader.js';
window.requestAnimationFrame = (function() {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 0);
    }
  );
})();

window.cancelAnimationFrame = (function() {
  return (
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.msCancelAnimationFrame ||
    function(intervalKey) {
      window.clearTimeout(intervalKey);
    }
  );
})();

class musicEx {
  constructor(props) {
    this.bgcolor = '#09112b';
    this.uniforms = null;
    this.objTotal = 128;
    this.stageW = window.innerWidth;
    this.stageH = window.innerHeight;
    this.borderSize = props.borderSize ? props.borderSize : 0;
    this.aspectRatio = props.aspectRatio ? props.aspectRatio : 1;
    const vals = {
      src: props.audio ? props.audio : audioFile
    };

    this.projectID = props.projectTitle ? props.projectTitle : 'musicEx';
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;
    this.startBtn.addEventListener('click', this.startAudio.bind(this));
    this.audAlyzer = new AudioHandler(vals);

    this.color = {};
    this.gradients = [
      {
        rotation: 315,
        colors: ['#8e160f', '#d3aa4d']
      },

      {
        rotation: 135,
        colors: ['#fdbedf', '#157fc2']
      },
      {
        rotation: 180,

        colors: ['#c9e013', '#9c07d5']
      },

      {
        rotation: 135,
        colors: ['#8d4034', '#7dfde4']
      },
      {
        rotation: 135,
        colors: ['#d1378b', '#44c6f3']
      },
      {
        rotation: 180,
        colors: ['#c79a84', '#44868d']
      },
      {
        rotation: 32,
        colors: ['#4adae9', '#e2ccaf']
      },
      {
        rotation: 180,
        colors: ['#07dced', '#912716']
      },

      {
        rotation: 180,
        colors: ['#dc8492', '#0a7e6e']
      },
      {
        rotation: 225,
        colors: ['#e7a09c', '#64a860']
      },
      {
        rotation: 135,
        colors: ['#1901b1', '#f13dc1']
      },
      {
        rotation: 135,
        colors: ['#826c83', '#f0f8b9']
      },
      {
        rotation: 135,
        colors: ['#1ac6fa', '#bd8e97']
      },
      {
        rotation: 135,
        colors: ['#9e029f', '#988c18']
      }
    ];
    this.objArray = [];

    this.siteRef = props.ref ? props.ref : Math.floor(Math.random() * this.gradients.length);
    console.log('this.siteRef', this.siteRef);
    this.canvas = this.createCanvas(true, this.projectID, ['fix', 'TL', 'overlay']);
    document.body.append(this.canvas);
    this.stage = new createjs.Stage(this.projectID);
    // this.init();
  }

  async init() {
    this.color.primary = this.gradients[this.siteRef].colors[0];
    this.color.secondary = this.gradients[this.siteRef].colors[1];
    this.onResize();
    this.buildScene();
    this.audAlyzer.initAndStart();
    this.renderFrame();
    // document.querySelector('#main').appendChild(this.pitchOutput);

    window.addEventListener('resize', this.onResize.bind(this), false);
  }
  buildScene() {
    // this.pitchOutput = this.createCanvas(false, 'pitchOut', ['abs', 'BR']);
    this.bg = new createjs.Shape();
    this.bg.graphics.beginFill(this.bgcolor).drawRect(0, 0, this.stageW, this.stageH);
    this.stage.addChild(this.bg);
    this.soundVector = new createjs.Shape();
    // this.soundVector.x = this.stageW / 2;
    // this.soundVector.regX = this.stageW / 2;
    // this.soundVector.scaleX = -1;
    for (var i = 0; i < this.objTotal; i++) {
      var x = (this.stageW / this.objTotal) * i;
      var y = this.stageH / 2;
      // var graphics = new createjs.Graphics()
      this.soundVector.graphics
        .beginFill('#fff')
        // .beginRadialGradientFill(['#fff', '#fff'], [1, 0], x, y, 0, x, y, 10)
        .drawCircle(x, y, 10);
    }

    this.stage.addChild(this.soundVector);
    this.stage.update();
    this.toggle = this.color.primary;
    // this.addShaders();
  }
  startAudio(evt) {
    this.init();
    evt.target.removeEventListener('click', this.startAudio.bind(this));
    this.startBtn.addEventListener('click', this.pauseAudio.bind(this));
  }
  pauseAudio(evt) {
    console.log('pause');
    this.stopAnimation();
    this.audAlyzer.pause();
    evt.target.removeEventListener('click', this.pauseAudio.bind(this));
    this.startBtn.addEventListener('click', this.playAudio.bind(this));
  }
  playAudio(evt) {
    this.audAlyzer.play();
    this.renderFrame();
    evt.target.removeEventListener('click', this.playAudio.bind(this));
    this.startBtn.addEventListener('click', this.pauseAudio.bind(this));
  }
  stopAnimation() {
    cancelAnimationFrame(this.aniTrack);
  }
  renderFrame() {
    this.aniTrack = requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
    this.stage.update();
  }
  createDots(data) {
    if (typeof data == 'undefined') {
      data = [];
      for (var i = 0; i < this.objTotal; i++) {
        data.push(Math.floor(Math.random() * 128));
      }
    }

    data = data.filter(a => {
      return a !== 0;
    });
    // var highlight = '#ffffff';
    var pri = '#f00';
    var sec = '#e00';
    var toggle = pri;
    
    this.soundVector.graphics.beginFill(this.bgcolor).drawRect(0, 0, this.stageW, this.stageH);
    for (var i = 0; i < data.length; i++) {
      var x = (this.stageW / data.length) * i;
      var y = this.stageH / 2;
      var radius = data[i];
      this.soundVector.graphics
        // .setStrokeStyle(radius/255)
        // .beginStroke(highlight)
        .beginFill(toggle)
        // .beginRadialGradientFill(['#fff', '#fff'], [1, 0], x, y, 0, x, y, radius)
        .drawCircle(x, y, radius);
      toggle = toggle == pri ? sec : pri;
    }
  }
  animateAudio() {
    this.audAlyzer.analyser.getByteFrequencyData(this.audAlyzer.dataArray);
    // this.heightMap = this.createDots(this.audAlyzer.dataArray);
    this.createDots(this.audAlyzer.dataArray);
  }

  onResize() {
    this.stageW = window.innerWidth;
    this.stageH = window.innerHeight;
    if (this.canvas) {
      this.canvas.width = this.stageW;
      this.canvas.height = this.stageH;
    }
    this.stage.update();
    // if (this.camera) {
    //   this.camera.aspect = window.innerWidth / window.innerHeight;
    //   this.camera.updateProjectionMatrix();
    //   this.renderer.setSize(window.innerWidth, window.innerHeight);
    // }
  }

  createCanvas(canvasFlag, id, classList) {
    const canvas = canvasFlag ? document.createElement('canvas') : document.createElement('div');
    canvas.setAttribute('id', id);
    classList.forEach((v, i) => {
      canvas.classList.add(v);
    });
    return canvas;
  }
  updateCanvasSize() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }

  toDec(val) {
    return val / 255;
  }
  indexOfMax(arr) {
    if (arr.length === 0) {
      return -1;
    }
    var max = arr[0];
    var maxIdx = 0;
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIdx = i;
        max = arr[i];
      }
    }
    return maxIdx;
  }

  addShaders() {
    this.heatVertex = `
    uniform sampler2D heightMap;
    uniform float heightRatio;
    varying vec2 vUv;
    varying float hValue;
    void main() {
      vUv = uv;
      vec3 pos = position;
      hValue = texture2D(heightMap, vUv).r;
      pos.y = hValue * heightRatio;
      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos,1.0);
    }
  `;
    this.heatFragment = `
    varying float hValue;
    
    // honestly stolen from https://www.shadertoy.com/view/4dsSzr
    vec3 heatmapGradient(float t) {
      return clamp((pow(t, 1.5) * 0.8 + 0.2) * vec3(smoothstep(0.0, 0.35, t) + t * 0.5, smoothstep(0.5, 1.0, t), max(1.0 - t * 1.7, t * 7.0 - 6.0)), 0.0, 1.0);
    }

    void main() {
      float v = abs(hValue - 1.);
      gl_FragColor = vec4(heatmapGradient(hValue), 1. - v * v) ;
    }
  `;
  }
}

export default musicEx;
