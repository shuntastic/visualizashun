import { mat4 } from './../libs/gl-matrix';
import AudioHandler from '../utils/AudioHandler';
import audioFile from '/assets/aud/BlueLace.mp3';
window.requestAnimationFrame = (function() {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 0);
    }
  );
})();

window.cancelAnimationFrame = (function() {
  return (
    window.cancelAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.msCancelAnimationFrame ||
    function(intervalKey) {
      window.clearTimeout(intervalKey);
    }
  );
})();

class ShaderExercise {
  constructor(props) {
    this.startBtn = props.startEl ? document.querySelector(props.startEl) : document.body;
    this.canvas = this.createCanvas();
    this.updateCanvas();
    this.gl = this.canvas.getContext('webgl');

    // If we don't have a GL context, give up now
    if (!this.gl) {
      alert('Unable to initialize Webthis.gl. Your browser or machine may not support it.');
      return;
    }

    this.gradients = [
      {
        rotation: 315,
        colors: [{ r: 142, g: 22, b: 15 }, { r: 211, g: 170, b: 77 }]
      },

      {
        rotation: 135,
        colors: [{ r: 253, g: 190, b: 223 }, { r: 21, g: 127, b: 194 }]
      },
      {
        rotation: 180,

        colors: [{ r: 201, g: 224, b: 19 }, { r: 156, g: 7, b: 23 }]
      },

      {
        rotation: 135,
        colors: [{ r: 141, g: 64, b: 52 }, { r: 125, g: 253, b: 228 }]
      },
      {
        rotation: 135,
        colors: [{ r: 209, g: 55, b: 139 }, { r: 68, g: 198, b: 243 }]
      },
      {
        rotation: 180,
        colors: [{ r: 199, g: 154, b: 132 }, { r: 68, g: 134, b: 141 }]
      },
      {
        rotation: 32,
        colors: [{ r: 74, g: 218, b: 233 }, { r: 226, g: 204, b: 175 }]
      },
      {
        rotation: 180,
        colors: [{ r: 7, g: 220, b: 237 }, { r: 145, g: 39, b: 2 }]
      },

      {
        rotation: 180,
        colors: [{ r: 220, g: 132, b: 146 }, { r: 10, g: 126, b: 110 }]
      },
      {
        rotation: 225,
        colors: [{ r: 231, g: 160, b: 156 }, { r: 100, g: 168, b: 96 }]
      },
      {
        rotation: 135,
        colors: [{ r: 25, g: 1, b: 177 }, { r: 241, g: 61, b: 93 }]
      },
      {
        rotation: 135,
        colors: [{ r: 130, g: 108, b: 131 }, { r: 240, g: 248, b: 185 }]
      },
      {
        rotation: 135,
        colors: [{ r: 26, g: 198, b: 250 }, { r: 189, g: 142, b: 151 }]
      },
      {
        rotation: 135,
        colors: [{ r: 158, g: 2, b: 159 }, { r: 152, g: 140, b: 24 }]
      }
    ];
    this.colorRef = this.gradients[
      parseInt(props.index ? props.index : Math.floor(Math.random() * this.gradients.length))
    ];
    this.colors = this.colorToMatrix(this.colorRef);

    /*    shaders     */

    // Vertex
    this.vsSource = `
        attribute vec4 aVertexPosition;
        attribute vec4 aVertexColor;
        uniform mat4 uModelViewMatrix;
        uniform mat4 uProjectionMatrix;
        varying lowp vec4 vColor;
        void main(void) {
            gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
            vColor = aVertexColor;
        }
        `;

    // Fragment

    this.fsSource = `
        varying lowp vec4 vColor;
        void main(void) {
            gl_FragColor = vColor;
        }
        `;

    this.shaderPrg = this.initShaderPrg(this.gl, this.vsSource, this.fsSource);

    this.programInfo = {
      program: this.shaderPrg,
      attribLocations: {
        vertexPosition: this.gl.getAttribLocation(this.shaderPrg, 'aVertexPosition'),
        vertexColor: this.gl.getAttribLocation(this.shaderPrg, 'aVertexColor')
      },
      uniformLocations: {
        projectionMatrix: this.gl.getUniformLocation(this.shaderPrg, 'uProjectionMatrix'),
        modelViewMatrix: this.gl.getUniformLocation(this.shaderPrg, 'uModelViewMatrix')
      }
    };
    window.addEventListener('resize', this.updateCanvas.bind(this));
    this.startBtn.addEventListener('click', this.startAudio.bind(this));
    document.querySelector('#main').appendChild(this.canvas);

    const vals = {
      src: audioFile
    };
    this.audAlyzer = new AudioHandler(vals);

    this.init();
  }

  init() {
    this.colorRef = this.gradients[Math.floor(Math.random() * this.gradients.length)];
    console.log('colorRef', this.colorRef);
    this.colors = this.colorToMatrix(this.colorRef);

    this.buffers = this.initBuffers(this.gl, this.colors);
    this.drawScene(this.gl, this.programInfo, this.buffers);
  }
  startAudio() {
    this.audAlyzer.initAndStart();
    this.renderFrame();
  }
  renderFrame() {
    requestAnimationFrame(this.renderFrame.bind(this));
    this.animateAudio();
  }
  animateAudio() {
    this.audAlyzer.analyser.getByteFrequencyData(this.audAlyzer.dataArray);
    let destColors = this.colorToMatrix(this.colorRef);

    for (var i = 0; i < this.audAlyzer.bufferLength; i++) {
      // console.log(i % 4);
      // console.log(this.audAlyzer.dataArray[i]);
      // let barWidth = this.audAlyzer.dataArray[i] * 0.025;
    }
        // console.log(destColors);
        destColors[3] = this.toDec(Math.max(this.audAlyzer.dataArray));
    this.buffers = this.initBuffers(this.gl,destColors);
    this.drawScene(this.gl, this.programInfo, this.buffers);
  }

  createCanvas() {
    const canvas = document.createElement('canvas');
    canvas.setAttribute('id', 'ShaderExercise');
    canvas.classList.add('abs');
    canvas.classList.add('TL');
    canvas.classList.add('overlay');
    return canvas;
  }
  updateCanvas() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }
  colorToMatrix(colorObj) {
    let toDec = val => {
      return val / 255;
    };

    let colorFrom = colorObj.colors[0];
    let colorTo = colorObj.colors[1];
    let output = [
      this.toDec(colorFrom.r),  // topRight RGBA
      this.toDec(colorFrom.g),
      this.toDec(colorFrom.b),
      1.0,
      this.toDec(colorFrom.r),  // topLeft
      this.toDec(colorFrom.g),
      this.toDec(colorFrom.b),
      1.0,
      this.toDec(colorTo.r),  // bottomRight RGBA
      this.toDec(colorTo.g),
      this.toDec(colorTo.b),
      1.0,
      this.toDec(colorTo.r),  // bottomLeft
      this.toDec(colorTo.g),
      this.toDec(colorTo.b),
      1.0
    ];
    return output;
  }
  initBuffers(gl, colors) {
    const positionBuffer = gl.createBuffer();
    this.gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    const positions = [1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, -1.0];

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    const colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    return {
      position: positionBuffer,
      color: colorBuffer
    };
  }

  drawScene(gl, programInfo, buffers) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Clear to black, fully opaque
    gl.clearDepth(1.0); // Clear everything
    gl.enable(gl.DEPTH_TEST); // Enable depth testing
    gl.depthFunc(gl.LEQUAL); // Near things obscure far things

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fieldOfView = (45 * Math.PI) / 180; // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    mat4.perspective(projectionMatrix, fieldOfView, aspect, zNear, zFar);

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.
    const modelViewMatrix = mat4.create();

    // Now move the drawing position a bit to where we want to
    // start drawing the square.

    mat4.translate(
      modelViewMatrix, // destination matrix
      modelViewMatrix, // matrix to translate
      [-0.0, 0.0, -1.38]
    );

    // Tell WebGL how to pull out the positions from the position
    // buffer into the vertexPosition attribute
    {
      const numComponents = 2;
      const type = gl.FLOAT;
      const normalize = false;
      const stride = 0;
      const offset = 0;
      gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
      gl.vertexAttribPointer(
        programInfo.attribLocations.vertexPosition,
        numComponents,
        type,
        normalize,
        stride,
        offset
      );
      gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);
    }

    // Tell WebGL how to pull out the colors from the color buffer
    // into the vertexColor attribute.
    {
      const numComponents = 4;
      const type = this.gl.FLOAT;
      const normalize = false;
      const stride = 0;
      const offset = 0;
      gl.bindBuffer(gl.ARRAY_BUFFER, buffers.color);
      gl.vertexAttribPointer(programInfo.attribLocations.vertexColor, numComponents, type, normalize, stride, offset);
      gl.enableVertexAttribArray(programInfo.attribLocations.vertexColor);
    }

    // Tell WebGL to use our program when drawing

    gl.useProgram(programInfo.program);

    // Set the shader uniforms

    gl.uniformMatrix4fv(programInfo.uniformLocations.projectionMatrix, false, projectionMatrix);
    gl.uniformMatrix4fv(programInfo.uniformLocations.modelViewMatrix, false, modelViewMatrix);

    {
      const offset = 0;
      const vertexCount = 4;
      this.gl.drawArrays(this.gl.TRIANGLE_STRIP, offset, vertexCount);
    }
  }

  /*
         shader program, so WebGL knows how to draw our data
    */

  initShaderPrg(gl, vsSource, fsSource) {
    const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

    // Create the shader program

    const shaderPrg = gl.createProgram();
    gl.attachShader(shaderPrg, vertexShader);
    gl.attachShader(shaderPrg, fragmentShader);
    gl.linkProgram(shaderPrg);

    // If creating the shader program failed, alert
    if (!gl.getProgramParameter(shaderPrg, gl.LINK_STATUS)) {
      alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderPrg));
      return null;
    }

    return shaderPrg;
  }

  loadShader(gl, type, source) {
    const shader = gl.createShader(type);

    // Send the source to the shader object
    gl.shaderSource(shader, source);
    // Compile the shader program

    gl.compileShader(shader);

    // See if it compiled successfully

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert('Shader Error: ' + gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
    }

    return shader;
  }

  // utils
  toDec(val) {
    return val / 255;
  }
  indexOfMax(arr) {
    if (arr.length === 0) {
      return -1;
    }
    var max = arr[0];
    var maxIdx = 0;
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIdx = i;
        max = arr[i];
      }
    }
    return maxIdx;
  }
}

export default ShaderExercise;
