import * as PIXI from 'pixi.js';
export function Building(index) {
    console.log('building' + index);
  var colorOpts = [
    { r: 255, g: 227, b: 255, hex: 0x518084 },
    { r: 178, g: 141, b: 178, hex: 0xd9afc7 },
    { r: 255, g: 243, b: 201, hex: 0x9e9660 },
    { r: 161, g: 204, b: 198, hex: 0x847f57 },
    { r: 150, g: 178, b: 174, hex: 0x3a5051 },
  ];
  var bldng = new PIXI.Sprite(
    this.loader.resources[window.bldngImageOptions[Math.floor(Math.random() * window.bldngImageOptions.length)]].texture
  );
  bldng.height = Math.random() * 300;
  bldng.width = bldng.height * Math.max(1.2, Math.random() * 2);
  bldng.x = 0;
  bldng.y = 0;
  bldng.alpha = 0.5;
  var rectangle = new PIXI.Graphics();
  var colorIndex = colorOpts[Math.min(colorOpts.length - 1, Math.floor(Math.random() * colorOpts.length))].hex;
  rectangle.beginFill(colorIndex);
  rectangle.drawRect(bldng.x, bldng.y, bldng.width, bldng.height);
  rectangle.endFill();
  var bldngContainer = new PIXI.Container();
  bldngContainer.addChild(rectangle);
  bldngContainer.addChild(bldng);
  bldngContainer.name = 'building' + index;
  return bldngContainer;
}
