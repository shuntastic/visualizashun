class AudioHandler {
  constructor(props) {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    // this.src = props.src;
    this.noteStrings = props.noteStrings || ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

    this.showDebug = props.showDebug || true;
    this.audio = new Audio(props.src);
    // this.audio.addEventListener('onloaded', evt => {
    // });
    this.audctx = null;
    this.analyser = null;
    this.src = null;
    this.bufferLength = null;
    this.dataArray = null;
  }
  init() {
    this.audio.load();
    this.audctx = new AudioContext();
    this.src = this.audctx.createMediaElementSource(this.audio);
    this.analyser = this.audctx.createAnalyser();
    this.src.connect(this.analyser);
    this.analyser.connect(this.audctx.destination);

    this.filter = this.audctx.createBiquadFilter();
    this.src.connect(this.filter);
    this.filter.connect(this.audctx.destination);

    this.analyser.fftSize = 512;
    this.bufferLength = this.analyser.frequencyBinCount;
    this.dataArray = new Uint8Array(this.bufferLength);
  }
  initAndStart() {
    this.init();
    this.audio.addEventListener('canplay', function() {
      this.play();
    });
  }

  initPitchDetect() {
    this.MIN_SAMPLES = 0; // will be initialized when AudioContext is created.
    this.GOOD_ENOUGH_CORRELATION = 0.9; // this is the "bar" for how close a correlation needs to be

    this.rafID = null;
    this.tracks = null;
    this.buflen = 1024;
    this.buf = new Float32Array(this.buflen);

    // this.noteStrings = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

    this.audctx = null;
    this.showDebug = true;
    this.isPlaying = false;
    this.sourceNode = null;
    this.analyser = null;
    this.theBuffer = null;
    this.DEBUGCANVAS = true;
    this.mediaStreamSource = null;
    this.detectorElem = null;
    this.canvasElem = null;
    this.waveCanvas = null;
    this.pitchElem = null;
    this.noteElem = null;
    this.detuneElem = null;
    this.detuneAmount = null;

    this.MAX_SIZE = Math.max(4, Math.floor(this.audctx.sampleRate / 5000)); // corresponds to a 5kHz signal
    var request = new XMLHttpRequest();
    request.open('GET', this.audio, true);
    request.responseType = 'arraybuffer';
    request.onload = function() {
      this.audctx.decodeAudioData(request.response, function(buffer) {
        this.theBuffer = buffer;
      });
    };
    request.send();
    if (this.showDebug) {
      this.showReport();
    }
  }

  play() {
    this.audio.play();
    this.audio.volume = 1.0;
  }
  pause() {
    this.audio.pause();
  }
  getBufferLength() {
    // this.bufferLength = this.analyser.frequencyBinCount;
    return this.bufferLength;
  }

  getDataArray() {
    return this.dataArray;
  }

  updateFrequencyData() {
    this.analyser.getByteFrequencyData(this.dataArray);
    return this.dataArray;
  }

  togglePlayback(evt) {
    if (this.isPlaying) {
      //stop playing and return
      this.sourceNode.stop(0);
      this.sourceNode = null;
      this.analyser = null;
      this.isPlaying = false;
      if (!window.cancelAnimationFrame) window.cancelAnimationFrame = window.webkitCancelAnimationFrame;
      window.cancelAnimationFrame(this.rafID);
      return;
      // return "start";
    }

    this.sourceNode = this.audctx.createBufferSource();
    this.sourceNode.buffer = this.theBuffer;
    this.sourceNode.loop = true;

    this.analyser = this.audctx.createAnalyser();
    this.analyser.fftSize = 2048;
    this.sourceNode.connect(this.analyser);
    this.analyser.connect(this.audctx.destination);
    this.sourceNode.start(0);
    this.isPlaying = true;
    this.isLiveInput = false;
    this.updatePitch();

    return;
    // return "stop";
  }
  // PITCH DETECT

  updatePitch(time) {
    var cycles = new Array();
    this.analyser.getFloatTimeDomainData(this.buf);
    var ac = this.autoCorrelate(this.buf, this.audctx.sampleRate);

    // TODO: Paint confidence meter on canvasElem here.

    var obj = {}
    if (this.DEBUGCANVAS) {
      // This draws the current waveform, useful for debugging
      this.waveCanvas.clearRect(0, 0, 512, 256);
      this.waveCanvas.strokeStyle = 'red';
      this.waveCanvas.beginPath();
      this.waveCanvas.moveTo(0, 0);
      this.waveCanvas.lineTo(0, 256);
      this.waveCanvas.moveTo(128, 0);
      this.waveCanvas.lineTo(128, 256);
      this.waveCanvas.moveTo(256, 0);
      this.waveCanvas.lineTo(256, 256);
      this.waveCanvas.moveTo(384, 0);
      this.waveCanvas.lineTo(384, 256);
      this.waveCanvas.moveTo(512, 0);
      this.waveCanvas.lineTo(512, 256);
      this.waveCanvas.stroke();
      this.waveCanvas.strokeStyle = 'black';
      this.waveCanvas.beginPath();
      this.waveCanvas.moveTo(0, buf[0]);
      for (var i = 1; i < 512; i++) {
        this.waveCanvas.lineTo(i, 128 + this.buf[i] * 128);
      }
      this.waveCanvas.stroke();
      obj.waveCanvas = this.waveCanvas;
    }
    if (ac == -1) {

      obj.confidence = 'vague';
    } else {
      obj.confidence = 'confident';
      this.pitch = ac;
      obj.pitch = Math.round(this.pitch);
      var note = this.noteFromPitch(this.pitch);
      this.noteElem.innerHTML = this.noteStrings[note % 12];
      obj.note = this.noteStrings[note % 12];
      var detune = this.centsOffFromPitch(this.pitch, note);
      if (detune == 0) {
        obj.detune = '';
        obj.detuneAmount = '--';
      } else {
        obj.detune = (detune < 0) ? 'flat' : 'sharp';
        obj.detuneAmount = Math.abs(detune);
      }
    }

    if (!window.requestAnimationFrame) window.requestAnimationFrame = window.webkitRequestAnimationFrame;
    this.rafID = window.requestAnimationFrame(this.updatePitch);
    return obj;
  }

  autoCorrelate(buf, sampleRate) {
    var SIZE = buf.length;
    var MAX_SAMPLES = Math.floor(SIZE / 2);
    var best_offset = -1;
    var best_correlation = 0;
    var rms = 0;
    var foundGoodCorrelation = false;
    var correlations = new Array(MAX_SAMPLES);

    for (var i = 0; i < SIZE; i++) {
      var val = buf[i];
      rms += val * val;
    }
    rms = Math.sqrt(rms / SIZE);
    if (rms < 0.01)
      // not enough signal
      return -1;

    var lastCorrelation = 1;
    for (var offset = this.MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
      var correlation = 0;

      for (var i = 0; i < MAX_SAMPLES; i++) {
        correlation += Math.abs(buf[i] - buf[i + offset]);
      }
      correlation = 1 - correlation / MAX_SAMPLES;
      correlations[offset] = correlation; // store it, for the tweaking we need to do below.
      if (correlation > this.GOOD_ENOUGH_CORRELATION && correlation > lastCorrelation) {
        foundGoodCorrelation = true;
        if (correlation > best_correlation) {
          best_correlation = correlation;
          best_offset = offset;
        }
      } else if (foundGoodCorrelation) {
        // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
        // Now we need to tweak the offset - by interpolating between the values to the left and right of the
        // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
        // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
        // (anti-aliased) offset.

        // we know best_offset >=1,
        // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and
        // we can't drop into this clause until the following pass (else if).
        var shift = (correlations[best_offset + 1] - correlations[best_offset - 1]) / correlations[best_offset];
        return sampleRate / (best_offset + 8 * shift);
      }
      lastCorrelation = correlation;
    }
    if (best_correlation > 0.01) {
      // console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
      return sampleRate / best_offset;
    }
    return -1;
    //	var best_frequency = sampleRate/best_offset;
  }

  noteFromPitch(frequency) {
    var noteNum = 12 * (Math.log(frequency / 440) / Math.log(2));
    return Math.round(noteNum) + 69;
  }

  frequencyFromNoteNumber(note) {
    return 440 * Math.pow(2, (note - 69) / 12);
  }

  centsOffFromPitch(frequency, note) {
    return Math.floor((1200 * Math.log(frequency / this.frequencyFromNoteNumber(note))) / Math.log(2));
  }

  showReport() {
    this.detectorElem = document.createElement('div');
    this.pitchElem = document.createElement('div');
    this.noteElem = document.createElement('div');
    this.detuneElem = document.createElement('div');
    this.detuneAmount = document.createElement('div');

    this.detectorElem.setAttribute('id', 'detector');
    this.pitchElem.setAttribute('id', 'pitch');
    this.noteElem.setAttribute('id', 'note');
    this.detuneElem.setAttribute('id', 'detune');
    this.detuneAmount.setAttribute('id', 'detune_amt');

    this.detectorElem.classList.add('abs TL');
    this.pitchElem.classList.add('abs TR');
    this.noteElem.classList.add('abs BL');
    this.detuneElem.classList.add('abs BR');
    this.detuneAmount.classList.add('abs BR');

    // this.canvasElem = document.createElement('div');
    // this.canvasElem.setAttribute('id', 'output' );
    // this.canvasElem.classList.add('abs TR');
  }

  //   UNUSED FOR NOW

  playStream(audioStream) {
    var uInt8Array = new Uint8Array(audioStream);
    var arrayBuffer = uInt8Array.buffer;
    var blob = new Blob([arrayBuffer]);
    var url = URL.createObjectURL(blob);

    this.audioElement.src = url;
    this.audioElement.play();
  }
  getAudioChannels() {
    this.audctx.decodeAudioData(someStereoBuffer, function(data) {
      var source = this.audctx.createBufferSource();
      source.buffer = data;
      var splitter = this.audctx.createChannelSplitter(2);
      source.connect(splitter);
      // var merger = this.audctx.createChannelMerger(2);
      // Reduce the volume of the left channel only
      // var gainNode = this.audctx.createGain();
      // gainNode.gain.value = 0.5;
      splitter.connect(gainNode, 0);
    });
  }
}
export default AudioHandler;
